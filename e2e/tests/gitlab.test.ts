import { Selector } from 'testcafe';
//var moment = require("moment");
fixture `Getting Started`
    .page `http://devexpress.github.io/testcafe/example`;

test('My first test', async t => {
    //const startDate = moment('1/1/2013').format("MMMM D YYYY").split(" ");
    await t
        .typeText('#developer-name', 'John Smith')
        .click('#submit-button')

        // Use the assertion to check if the actual header text is equal to the expected one
        .expect(Selector('#article-header').innerText).eql('Thank you, Joh3434n Smith!');
});
